function [conPeak1, conPeak2] = fn_compute_condensin_peaks(conStackStr, spotRad)

[dx, nFinSlices, ~] = size(conStackStr);
conPeak1 = zeros(dx,nFinSlices);
conPeak2 = zeros(dx,nFinSlices);


for i = 1: nFinSlices
    curSlice = squeeze(conStackStr(:,i,:));
    %figure; imagesc(curSlice);
    curSlice = permute(curSlice,[2 1]);
    %figure; imagesc(curSlice);
    
    curSliceMax1D = max(curSlice);
    
    [maxVal1, idx1] = max(curSliceMax1D);
    %figure; plot(curSliceMax1D);
    conPeak1(idx1,i) = maxVal1;
    if idx1>spotRad && idx1 <dx-spotRad
        curSliceMax1D(idx1-spotRad:idx1+spotRad) = 0;
    end
    
    [maxVal2, idx2] = max(curSliceMax1D);
    %figure; plot(curSliceMax1D);
    conPeak2(idx2,i) = maxVal2;
end
end