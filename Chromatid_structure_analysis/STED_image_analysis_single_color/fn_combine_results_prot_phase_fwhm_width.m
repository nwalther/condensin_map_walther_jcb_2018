function fn_combine_results_prot_phase_fwhm_width(inDir, outDir, protName, phaseName, procAll, selRegFlag, computeFlag, paramsDir)
%This function combines the results from individual cell regions and
%caculate FWHM width of each subunit per mitotic stage
%inDir: input directory
%outDir: output directoy
%protName: name of the protein
%phaseName: name of the mitotic phase
%procAll: 1: process all regions
%selRegFlag: Process region with particular flag type
%computFlag: Processing type - parameters 1 or width 2
%clear all;
close all;
crosRad = 80;
voxelSizeX = 20;
eqLenConPeak = zeros(161, 400);
maxShift = 25;
addUpLenPix = 2000/voxelSizeX; % number of pixels in two micrometer dist
% computeFlag = 1 - generating excel file with all params only
% computeFlag = 2 - generating radial distribution only

if ~exist(outDir)
    mkdir(outDir);
end

for protIdx = 1: length(protName)
    for phaseIdx = 1: length(phaseName)
        meanTotInt = [];
        meanTotIntChr = [];
        meanTotIntSeg = [];
        meanWidthCon  = [];
        meanWidthConSeg = [];
        meanWidthChr = [];
        meanWidthConSecond = [];
        conPeak = [];
        conYLen = [];
        histLeft = histc(0, 1:maxShift);
        histRight = histc(0, 1:maxShift);
        histMiddle = histc(0, 1:maxShift);
        
        conCrosDist = zeros(2*crosRad+1, 2*crosRad+1);
        chrCrosDist = zeros(2*crosRad+1, 2*crosRad+1);
        totProjCon = zeros(2*crosRad+1,1);
        totProjChr = zeros(2*crosRad+1,1);
        totProjConSeg = zeros(2*crosRad+1,1);
        hmfwChr = [];
        hmfwCon = [];
        regionDir = dir([inDir '*' protName{protIdx} '*' phaseName{phaseIdx} '*']);
        numRegions = 0;
        disp(['Processing flag:' num2str(selRegFlag) ' Prot_' protName{protIdx} ' Phase_' phaseName{phaseIdx}]);
        for regionDirIdx = 1: length(regionDir)
            if regionDir(regionDirIdx).isdir == 1
                curRegionDir = fullfile(inDir, regionDir(regionDirIdx).name, filesep);
                fpRegFlag = fopen([curRegionDir 'region_type_flag.txt'],'r');
                if fpRegFlag == -1
                    continue;
                else
                    curLine = fgets(fpRegFlag);
                    regFlag = sscanf(curLine, '%d');
                end
                
                if (procAll == 0 && regFlag ==selRegFlag) || (procAll == 1 && regFlag >0)
                                                           
                    fnListMat = dir([curRegionDir 'Extracted_Params' filesep regionDir(regionDirIdx).name '.mat']) ; %Name of the input file - chromatin
                    fnConListTif = dir([curRegionDir 'Disp_Stack_up' filesep '*Cond*.tif']) ; %Name of the input file - chromatin
                    fnChrListTif = dir([curRegionDir 'Disp_Stack_up' filesep '*Chr*.tif']) ; %Name of the input file - chromatin
                    
                    if length(fnListMat)>0 && (computeFlag == 1 || computeFlag ==3)
                        curMat = load([curRegionDir 'Extracted_Params' filesep fnListMat(1).name]);
                        
                        numRegions = numRegions + 1;
                        meanTotInt(numRegions)    = curMat.meanTotInt;
                        meanTotIntChr(numRegions) = curMat.meanTotIntChr;
                        meanTotIntSeg(numRegions) = curMat.meanTotIntSeg;
                        meanWidthCon(numRegions)  = curMat.meanWidthCon;
                        meanWidthConSeg(numRegions) = curMat.meanWidthConSeg;
                        meanWidthChr(numRegions)  = curMat.meanWidthChr;
                        meanWidthConSecond(numRegions) = curMat.meanWidthConSecond;
                        
                        curConPeak = curMat.conPeak1 + curMat.conPeak2;
                        conYLen(numRegions) = size(curConPeak,2);
                        eqLenConPeak(:,:) = 0;
                        eqLenConPeak(:,1:conYLen(numRegions)) = curConPeak; 
                        conPeak(:,:,numRegions) = eqLenConPeak;
                        
                        %Shifting the values to the peak before adding up 
                        [~,idx] = max(curMat.totProjCon);
                        shiftedCon = circshift(curMat.totProjCon, ceil(length(curMat.totProjCon)/2)-idx);
                        totProjCon = totProjCon + shiftedCon;
                        
                        [~,idx] = max(curMat.totProjConSeg);
                        shiftedConSeg = circshift(curMat.totProjConSeg, ceil(length(curMat.totProjConSeg)/2)-idx);
                        totProjConSeg = totProjConSeg + shiftedConSeg;
                        
                        [~,idx] = max(curMat.totProjChr);
                        shiftedChr = circshift(curMat.totProjChr, ceil(length(curMat.totProjChr)/2)-idx);
                        totProjChr = totProjChr + shiftedChr;
                        
                        totIntConMaskLeft = curMat.totIntConMaskLeft;
                        totIntConMaskLeft = (totIntConMaskLeft- min(totIntConMaskLeft))/(max(totIntConMaskLeft)-min(totIntConMaskLeft));
                        [pks,locs] = findpeaks(totIntConMaskLeft, 'MinPeakDistance', 3, 'MinPeakProminence',0.03);
                        locsDist = diff(locs);
                        histLeft = histLeft + histc(locsDist, 1:maxShift);
                        
                        totIntConMaskRight = curMat.totIntConMaskRight;
                        totIntConMaskRight = (totIntConMaskRight- min(totIntConMaskRight))/(max(totIntConMaskRight)-min(totIntConMaskRight));
                        [pks,locs] = findpeaks(totIntConMaskRight, 'MinPeakDistance', 3, 'MinPeakProminence',0.03);
                        locsDist = diff(locs);
                        histRight = histRight + histc(locsDist, 1:maxShift);
                        
                        totIntConMaskMiddle = curMat.totIntConMaskMiddle;
                        totIntConMaskMiddle = (totIntConMaskMiddle- min(totIntConMaskMiddle))/(max(totIntConMaskMiddle)-min(totIntConMaskMiddle));
                        [pks,locs] = findpeaks(totIntConMaskMiddle, 'MinPeakDistance', 3, 'MinPeakProminence',0.03);
                        locsDist = diff(locs);
                        histMiddle = histMiddle + histc(locsDist, 1:maxShift);

                        
                    end
                    if  (length(fnConListTif)>0 && length(fnChrListTif)>0) && (computeFlag ==2 || computeFlag == 3)
                        [conStack]   = fn_open_tiff_stack([curRegionDir 'Disp_Stack_up' filesep], fnConListTif(1).name);
                        [chrStack]   = fn_open_tiff_stack([curRegionDir 'Disp_Stack_up' filesep], fnChrListTif(1).name);
                        
                        conSlice = squeeze(sum(conStack, 2));
                        conSlice1 = permute(conSlice, [2 1]);
                        conSlice1 = conSlice1 * (1000/(voxelSizeX*size(conStack,2)));
                        conCrosDist = conCrosDist + conSlice1;
                        %profCon = max(conSlice1);
                        profCon = sum(conSlice1);
                        
                        chrSlice = squeeze(sum(chrStack, 2));
                        chrSlice1 = permute(chrSlice, [2 1]);
                        chrSlice1 = chrSlice1 * (1000/(voxelSizeX*size(chrStack,2)));
                        chrCrosDist = chrCrosDist + chrSlice1;
                        %profChr = max(chrSlice1);
                        profChr = sum(chrSlice1);
                        if computeFlag == 2
                            numRegions = numRegions+1;
                        end
                        %%
                        lenConPix = size(conStack,2);
                        if lenConPix < addUpLenPix
                            hmfwCon(numRegions) = fwhm(1:length(profCon),profCon);
                            hmfwChr(numRegions) = fwhm(1:length(profChr),profChr);
                        else
                            lenCon = 0;
                            lenChr = 0;
                            numSeg = 0;
                            for axIdx = 1: 10: lenConPix-addUpLenPix +1
                                %disp(axIdx);
                                conSlicePerMic = squeeze(sum(conStack(:,axIdx:axIdx+addUpLenPix-1,:), 2));
                                conSlice1PerMic = permute(conSlicePerMic, [2 1]);
                                profConPerMic = sum(conSlice1PerMic);
                                lenCon = lenCon + fwhm(1:length(profConPerMic),profConPerMic);
                                
                                chrSlicePerMic = squeeze(sum(chrStack(:,axIdx:axIdx+addUpLenPix-1,:), 2));
                                chrSlice1PerMic = permute(chrSlicePerMic, [2 1]);
                                profChrPerMic = sum(chrSlice1PerMic);
                                lenChr = lenChr + fwhm(1:length(profChrPerMic),profChrPerMic);
                                numSeg = numSeg + 1;
                            end
                            lenCon = lenCon/numSeg;
                            lenChr = lenChr/numSeg;
                            hmfwCon(numRegions) = lenCon;
                            hmfwChr(numRegions) = lenChr;
                        end
                    end
                end
            end
        end
        
        if numRegions>0 && (computeFlag == 1 || computeFlag ==3)
            meanMeanTotInt = mean(meanTotInt);
            stdMeanTotInt  = std(meanTotInt);
            
            meanMeanTotIntChr = mean(meanTotIntChr);
            stdMeanTotIntChr  = std(meanTotIntChr);
            
            meanMeanTotIntSeg = mean(meanTotIntSeg);
            stdMeanTotIntSeg  = std(meanTotIntSeg);
            
            meanMeanWidthCon = mean(meanWidthCon);
            stdMeanWidthCon  = std(meanWidthCon);
            
            meanMeanWidthConSeg = mean(meanWidthConSeg);
            stdMeanWidthConSeg = std(meanWidthConSeg);
            
            meanMeanWidthChr = mean(meanWidthChr);
            stdMeanWidthChr  = std(meanWidthChr);
            
            meanMeanWidthConSecond = mean(meanWidthConSecond);
            stdMeanWidthConSecond  = std(meanWidthConSecond);
            
            totProjCon = totProjCon/numRegions;
            totProjConEq = fn_equalize_and_normalize_intensity_profie(totProjCon);

            totProjConSeg = totProjConSeg/numRegions;
            totProjConSegEq = fn_equalize_and_normalize_intensity_profie(totProjConSeg);

            totProjChr = totProjChr/numRegions;
            totProjChrEq = fn_equalize_and_normalize_intensity_profie(totProjChr);

            %%Detecting peak and then cut it off
            
            %%
            foldTotProjCon = zeros(crosRad+1,1);
            foldTotProjConSeg = zeros(crosRad+1,1);
            foldTotProjChr = zeros(crosRad+1,1);
            
            j = crosRad + 1;
            for i = 1:crosRad+1
                foldTotProjCon(i) = totProjCon(crosRad+i)+ totProjCon(j);
                foldTotProjConSeg(i) = totProjConSeg(crosRad+i)+totProjConSeg(j);
                foldTotProjChr(i) = totProjChr(crosRad+i)+totProjChr(j);
                j = j - 1;
            end
            
            lenProf = length(totProjConEq);
            x = -floor(lenProf/2):floor(lenProf/2);
            x = x * 20;
            h = figure('NumberTitle','off', 'Visible','off'); plot(x,totProjConEq, 'LineWidth',2);
            xlabel('Distance from condensin axis (nm)');
            ylabel('Normalized intensity (au)');
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_norm_int_prof.tif'];
            saveas(h, filename, 'tif');
            close all;
            
            conMaxWidth_nm = 589.7782342; %gfpTOP2Ac102-ana-early
            halfConMaxWidth_pix = round(conMaxWidth_nm/40);
            conMinWidth_nm = 311.593054; %NCAPD3gfpc16-prometa
            halfConMinWidth_pix = round(conMinWidth_nm/40);
            
            perInsideMaxWidth = sum(totProjCon(ceil(lenProf/2)-halfConMaxWidth_pix:ceil(lenProf/2)+halfConMaxWidth_pix))/sum(totProjCon);
            perInsideMinWidth = sum(totProjCon(ceil(lenProf/2)-halfConMinWidth_pix:ceil(lenProf/2)+halfConMinWidth_pix))/sum(totProjCon);

            %Allign conPeaks
%             [allignConPeakSum, dtAllignConPeakSum] = fn_allignConPeaksAndGenAverage(conPeak, conYLen, maxShift);
%             h = figure('Name','Mean cross section intensity distribution of condensin', 'NumberTitle','off', 'Visible','off'); imagesc(allignConPeakSum(41:120,:)); axis equal; axis tight; colorbar
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Cond_Peak_Sum.tif'];
%             saveas(h, filename, 'tif');
%             
%             combDtAllignImage = (dtAllignConPeakSum(41:120,:));
%             maxVal = max(combDtAllignImage(:));
%             
%             combDtAllignImage = maxVal - combDtAllignImage;
%             h = figure('Name','Mean cross section intensity distribution of condensin', 'NumberTitle','off', 'Visible','off'); imagesc(combDtAllignImage); axis equal; axis tight; colorbar
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Cond_Peak_Dt_Transform.tif'];
%             saveas(h, filename, 'tif');
%             
%             h = figure('NumberTitle','off', 'Visible','off'); bar(1:maxShift, histLeft);
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Hist_Peak_Dist_Left.tif'];
%             saveas(h, filename, 'tif');
%             h = figure('NumberTitle','off', 'Visible','off'); bar(1:maxShift, histRight);
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Hist_Peak_Dist_Right.tif'];
%             saveas(h, filename, 'tif');
%              h = figure('NumberTitle','off', 'Visible','off'); bar(1:maxShift, histMiddle);
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Hist_Peak_Dist_Middle.tif'];
%             saveas(h, filename, 'tif');
%             close all;
            %Renaming the variables before saving into the mat files
            meanTotInt = meanMeanTotInt;
            stdTotInt  = stdMeanTotInt;
            
            meanTotIntChr = meanMeanTotIntChr;
            stdTotIntChr = stdMeanTotIntChr;
            
            meanTotIntSeg = meanMeanTotIntSeg;
            stdTotIntSeg = stdMeanTotIntSeg;
            
            meanWidthCon = meanMeanWidthCon;
            stdWidthCon = stdMeanWidthCon;
            
            meanWidthConSeg = meanMeanWidthConSeg;
            stdWidthConSeg = stdMeanWidthConSeg;
            
            meanWidthChr = meanMeanWidthChr;
            stdWidthChr  = stdMeanWidthChr;
            
            meanWidthConSecond = meanMeanWidthConSecond;
            stdWidthConSecond  = stdMeanWidthConSecond;
            
            %outDir = inDir;
            xlsFileName = [protName{protIdx} '_' phaseName{phaseIdx} '.xls'];
            
            xlswrite([outDir xlsFileName], {'Cond_MeanTotInt'}, 1, strcat('A', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdTotInt'}, 1, strcat('B', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_MeanTotIntSeg'}, 1, strcat('C', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdTotIntSeg'}, 1, strcat('D', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'Cond_MeanWidth'}, 1, strcat('E', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdWidth'}, 1, strcat('F', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_MeanWidthSeg'}, 1, strcat('G', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_StdWidthSeg'}, 1, strcat('H', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_MeanTotInt'}, 1, strcat('I', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_StdTotInt'}, 1, strcat('J', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_MeanWidth'}, 1, strcat('K', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_StdWidth'}, 1, strcat('L', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'Con_MeanWidthSecond'}, 1, strcat('M', num2str(1)));
            xlswrite([outDir xlsFileName], {'Con_StdWidthSecond'}, 1, strcat('N', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'Chr_RadIntPerMicrometer'}, 1, strcat('O', num2str(1)));
            xlswrite([outDir xlsFileName], {'Con_RadIntPerMicrometer'} , 1, strcat('P', num2str(1)));
            xlswrite([outDir xlsFileName], {'Con_RadIntDSegPerMicrometer'} , 1, strcat('Q', num2str(1)));
            
            xlswrite([outDir xlsFileName], {'PerConInsideMaxWidth'}, 1, strcat('R', num2str(1)));
            xlswrite([outDir xlsFileName], {'PerConInsideMinWidth'} , 1, strcat('S', num2str(1)));
            %xlswrite([outDir xlsFileName], {'Con_RadIntDistSegFolded'} , 1, strcat('T', num2str(1)));
            xlswrite([outDir xlsFileName], {'Num_Regions_Combined'} , 1, strcat('T', num2str(1)));
            
            xlswrite([outDir xlsFileName], meanTotInt, 1, ['A' num2str(2) ': A' num2str(2)]);
            xlswrite([outDir xlsFileName], stdTotInt, 1, ['B' num2str(2) ': B' num2str(2)]);
            xlswrite([outDir xlsFileName], meanTotIntSeg, 1, ['C' num2str(2) ': C' num2str(2)]);
            xlswrite([outDir xlsFileName], stdTotIntSeg, 1, ['D' num2str(2) ': D' num2str(2)]);
            
            xlswrite([outDir xlsFileName], meanWidthCon, 1, ['E' num2str(2) ': E' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthCon, 1, ['F' num2str(2) ': F' num2str(2)]);
            xlswrite([outDir xlsFileName], meanWidthConSeg, 1, ['G' num2str(2) ': G' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthConSeg, 1, ['H' num2str(2) ': H' num2str(2)]);
            
            xlswrite([outDir xlsFileName], meanTotIntChr, 1, ['I' num2str(2) ': I' num2str(2)]);
            xlswrite([outDir xlsFileName], stdTotIntChr, 1,  ['J' num2str(2) ': J' num2str(2)]);
            
            xlswrite([outDir xlsFileName], meanWidthChr, 1, ['K' num2str(2) ': K' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthChr, 1, ['L' num2str(2) ': L' num2str(2)]);
            
            xlswrite([outDir xlsFileName], meanWidthConSecond, 1, ['M' num2str(2) ': M' num2str(2)]);
            xlswrite([outDir xlsFileName], stdWidthConSecond, 1, ['N' num2str(2) ': N' num2str(2)]);
            
            xlswrite([outDir xlsFileName], totProjChrEq, 1, ['O' num2str(2) ': O' num2str(2*crosRad+2)]);
            xlswrite([outDir xlsFileName], totProjConEq, 1, ['P' num2str(2) ': P' num2str(2*crosRad+2)]);
            xlswrite([outDir xlsFileName], totProjConSegEq, 1, ['Q' num2str(2) ': Q' num2str(2*crosRad+2)]);
            
            xlswrite([outDir xlsFileName], perInsideMaxWidth, 1, ['R' num2str(2) ': R' num2str(crosRad+2)]);
            xlswrite([outDir xlsFileName], perInsideMinWidth, 1, ['S' num2str(2) ': S' num2str(crosRad+2)]);
            %xlswrite([outDir xlsFileName], foldTotProjConSeg, 1, ['T' num2str(2) ': T' num2str(crosRad+2)]);
            xlswrite([outDir xlsFileName], numRegions, 1, ['T' num2str(2) ': U' num2str(2)]);
            
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '.mat'];
            save(filename, 'meanTotInt', 'meanTotIntSeg', 'meanWidthCon', 'meanWidthConSeg', 'meanWidthChr', 'meanTotIntChr', 'meanWidthConSecond', 'stdTotInt', 'stdTotIntSeg', 'stdWidthCon', 'stdWidthConSeg', 'stdTotIntChr', 'stdWidthChr', 'stdWidthChr', 'meanWidthConSecond', 'totProjCon', 'totProjConSeg', 'totProjChr', 'numRegions');
        end
        
        if numRegions>0 && (computeFlag == 2 || computeFlag ==3)
            conCrosDist = conCrosDist/numRegions;
            chrCrosDist = chrCrosDist/numRegions;
            hmfwCon = hmfwCon * voxelSizeX;
            hmfwChr = hmfwChr * voxelSizeX;
            meanHmfwCon = mean(hmfwCon);
            stdHmfwCon  = std(hmfwCon);
            meanHmfwChr = mean(hmfwChr);
            stdHmfwChr  = std(hmfwChr);
            %hmfwChr = hmfwChr/numRegions * voxelSizeX;
            %hmfwCon = hmfwCon/numRegions * voxelSizeX;
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Cond.tif'];
%             imwrite(uint16(conCrosDist), filename, 'tif');
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Chr.tif'];
%             imwrite(uint16(chrCrosDist), filename, 'tif');
            
%             h = figure('Name','Mean cross section intensity distribution of condensin', 'NumberTitle','off', 'Visible','off'); imagesc(conCrosDist, [0 intLimCon]); axis equal; axis tight; colorbar
%             ax=gca;
%             set(ax,'XTickLabel', {'400', '800', '1200', '1600', '2000', '2400', '2800', '3200'});
%             set(ax,'YTickLabel', {'400', '800', '1200', '1600', '2000', '2400', '2800', '3200'})
%             saveas(h,[outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Cond_color.tif'], 'tif');
%             
%             h = figure('Name','Mean cross section intensity distribution of chromosome','NumberTitle','off', 'Visible','off'); imagesc(chrCrosDist, [0 intLimChr]); axis equal; axis tight; colormap(gray); colorbar
%             ax=gca;
%             set(ax,'XTickLabel', {'400', '800', '1200', '1600', '2000', '2400', '2800', '3200'});
%             set(ax,'YTickLabel', {'400', '800', '1200', '1600', '2000', '2400', '2800', '3200'})
%             saveas(h,[outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Chr_color.tif'], 'tif');
%             
            lenParams = length(paramsDir);
            curParamsDir = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '.mat'];
            curParamsDir(1:lenParams) = paramsDir;
            
            paramsMat = load(curParamsDir);
            
            initCentCon = centerOfMass(conCrosDist);
            %meanWidthCon = paramsMat.meanWidthCon;
            meanWidthCon = meanHmfwCon; %%modified on 2017_11_10
            curRadCon = round(meanWidthCon/2/voxelSizeX);
            
%             conCrosDistIso = fn_GetIsotropic_Dist(conCrosDist, initCentCon, curRadCon, isoDx, isoDy);
%             xLineCon = conCrosDistIso(ceil(isoDx/2),:);
%             
%             h = figure('Name','Mean cross section intensity distribution of condensin - isotropic','NumberTitle','off', 'Visible','off'); imagesc(conCrosDistIso); axis equal; axis tight; 
%             ax=gca;
%             set(ax, 'XTickLabel', {'200', '400', '600', '800', '1000', '1200', '1400', '1600', '1800', '2000'});
%             set(ax, 'YTickLabel', {'200', '400', '600', '800', '1000', '1200', '1400', '1600', '1800', '2000'});
%             fnColCon = [protName{protIdx} '_' phaseName{phaseIdx} '_Cond_color_iso.tif'];
%             saveas(h,[outDir  fnColCon], 'tif');
%             
%             initCentChr = centerOfMass(chrCrosDist);
%             
%             if mod(protIdx,2) == 1
%                 meanWidthChr =1121.497063;
%             else
%                 meanWidthChr =1101.145742;
%             end
% 
%             %meanWidthChr = paramsMat.meanWidthChr;
%             curRadChr = round(meanWidthChr/2/voxelSizeX);
%             
%             chrCrosDistIso = fn_GetIsotropic_Dist(chrCrosDist, initCentChr, curRadChr, isoDx, isoDy);
%             xLineChr = chrCrosDistIso(ceil(isoDx/2),:);
%             h = figure('Name','Mean cross section intensity distribution of condensin - isotropic','NumberTitle','off', 'Visible','off'); imagesc(chrCrosDistIso); axis equal; axis tight; colormap(gray); 
%             ax=gca;            
%             set(ax, 'XTickLabel', {'200', '400', '600', '800', '1000', '1200', '1400', '1600', '1800', '2000'});
%             set(ax, 'YTickLabel', {'200', '400', '600', '800', '1000', '1200', '1400', '1600', '1800', '2000'});
%             fnColChr = [protName{protIdx} '_' phaseName{phaseIdx} '_Chr_color_iso.tif'];
%             saveas(h,[outDir  fnColChr], 'tif');
%             
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Cond_Iso.tif'];
%             imwrite(uint16(conCrosDistIso), filename, 'tif');
%             filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '_Chr_Iso.tif'];
%             imwrite(uint16(chrCrosDistIso), filename, 'tif');
%             
%             fn_save_combined_distribution_image(outDir, fnColCon, fnColChr);
            
%             maxIntCon = max(max(conCrosDistIso));
%             maxIntChr = max(max(chrCrosDistIso));
            filename = [outDir  protName{protIdx} '_' phaseName{phaseIdx} '.mat'];
            save(filename, 'hmfwCon', 'hmfwChr', 'meanHmfwCon', 'meanHmfwChr',  'stdHmfwCon', 'stdHmfwChr', 'numRegions');
            xlsFileName = [protName{protIdx} '_' phaseName{phaseIdx} '.xls'];
            
            xlswrite([outDir xlsFileName], {'Cond_Indiv_HMFW'}, 1, strcat('A', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_Mean_HMFW'}, 1, strcat('B', num2str(1)));
            xlswrite([outDir xlsFileName], {'Cond_Stdev_HMFW'},  1, strcat('C', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_Indiv_HMFW'}, 1, strcat('D', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_Mean_HMFW'}, 1, strcat('E', num2str(1)));
            xlswrite([outDir xlsFileName], {'Chr_Stdev_HMFW'},  1, strcat('F', num2str(1)));
            xlswrite([outDir xlsFileName], {'Num_Regions'},  1, strcat('G', num2str(1)));
            
            xlswrite([outDir xlsFileName], hmfwCon', 1, ['A' num2str(2) ': A' num2str(length(hmfwCon)+1)]);
            xlswrite([outDir xlsFileName], meanHmfwCon, 1, ['B' num2str(2) ': B' num2str(2)]);
            xlswrite([outDir xlsFileName], stdHmfwCon,  1, ['C' num2str(2) ': C' num2str(2)]);
            xlswrite([outDir xlsFileName], hmfwChr', 1, ['D' num2str(2) ': D' num2str(length(hmfwChr)+1)]);
            xlswrite([outDir xlsFileName], meanHmfwChr, 1, ['E' num2str(2) ': E' num2str(2)]);
            xlswrite([outDir xlsFileName], stdHmfwChr,  1, ['F' num2str(2) ': F' num2str(2)]);
            xlswrite([outDir xlsFileName], numRegions,  1, ['G' num2str(2) ': G' num2str(2)]);
%             xlswrite([outDir xlsFileName], maxIntCon,  1, ['H' num2str(2) ': H' num2str(2)]);
%             xlswrite([outDir xlsFileName], maxIntChr,  1, ['I' num2str(2) ': I' num2str(2)]);
%             xlswrite([outDir xlsFileName], xLineCon', 1, ['H' num2str(2) ': H' num2str(isoDx+2)]);
%             xlswrite([outDir xlsFileName], xLineChr', 1, ['I' num2str(2) ': I' num2str(isoDx+2)]);
        end
        
        clear meanTotInt;
        clear meanTotIntChr;
        clear meanTotIntSeg;
        clear meanWidthCon;
        clear meanWidthChr;
       
        fclose all;
    end
end