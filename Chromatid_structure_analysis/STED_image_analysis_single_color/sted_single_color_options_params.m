function [opt] = sted_single_color_options_params()
%Voxel sizes in nano meter
opt.voxelSizeX = 20; 
opt.voxelSizeY = 20;
opt.voxelSizeZ = 20; %Interpolated 
opt.crosRad = 80; %Radius for slice extraction
opt.factGlb =0.4; % Contributing factor for global thresholds
opt.sigma = 0.5;  % Sigma for Gaussian filter - condensin
opt.hsize = 1;    % Kernel size of Gaussian filter - condensin
opt.upSample = 256;
opt.stepSizeInit = 5; % Slicing step size in pixel - initial
opt.stepSizeFin = 1;  % Sliceing ste size in pixel - final
opt.reproc = 1; % 1 - reprocessing of all data, 0 otherwise
opt.spotRad = 2;

opt.chrInitRef = 0; %Set 0 if Condensin is to be used as initial centroid ref.
opt.eigValPortion = 1.35; %Portion of the eigen value to truncate cond region to be analysed
end
