function [regConStack, regChrStack] = fn_register_all_slices(inConStack, inChrStack, refZIdx, voxelSizeX, voxelSizeY)
%Register all the slices starting with refZIdx
Nz= size(inConStack,3);
regConStack = zeros(size(inConStack));
regChrStack = zeros(size(inChrStack));
[optimizer,metric] = imregconfig('multimodal');

regConStack(:,:,refZIdx) = inConStack(:,:,refZIdx);
regChrStack(:,:,refZIdx) = inChrStack(:,:,refZIdx);


for inZIdx = refZIdx+1:Nz
    Rref = imref2d(size(regConStack(:,:,inZIdx-1)), voxelSizeY, voxelSizeX);
    Rin  = imref2d(size(inConStack(:,:,inZIdx)) ,  voxelSizeY, voxelSizeX);
    
    geomtform = imregtform(inConStack(:,:,inZIdx), Rin, regConStack(:,:,inZIdx-1), Rref, 'rigid', optimizer, metric);
    
    regConStack(:,:,inZIdx) = imwarp(inConStack(:,:,inZIdx),Rin,geomtform,'bicubic','OutputView',Rref);
    regChrStack(:,:,inZIdx) = imwarp(inChrStack(:,:,inZIdx),Rin,geomtform,'bicubic','OutputView',Rref);
end

for inZIdx = refZIdx-1:-1:1
    Rref = imref2d(size(regConStack(:,:,inZIdx+1)), voxelSizeY, voxelSizeX);
    Rin  = imref2d(size(inConStack(:,:,inZIdx))   , voxelSizeY, voxelSizeX);
    
    geomtform = imregtform(inConStack(:,:,inZIdx), Rin, regConStack(:,:,inZIdx+1), Rref, 'rigid', optimizer, metric);
    
    regConStack(:,:,inZIdx) = imwarp(inConStack(:,:,inZIdx),Rin,geomtform,'bicubic','OutputView',Rref);
    regChrStack(:,:,inZIdx) = imwarp(inChrStack(:,:,inZIdx),Rin,geomtform,'bicubic','OutputView',Rref);
end
end