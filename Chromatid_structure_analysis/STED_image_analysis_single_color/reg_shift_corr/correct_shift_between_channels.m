%This script corrects any drift between two channels both for registerred
%image and its segmented binary mask
%The amount of drift is taken as input
%DNA channel is shifted with respect to condensin

%Author: Julius Hossain, EMBL, Heidelberg
%Last update: 2018-03-05

%clc;
clear all

%Get the root directory of sour code
[src_root_dir,~,~]=fileparts(pwd);
[src_root_dir,~,~]=fileparts(src_root_dir); %Go one more stage up
%Add path of related common functions required
addpath(fullfile(src_root_dir, 'common_functions'));

reProcShift = 1; % 1 - reprocess the data, 0 otherwise

%Input directory
inDir = fullfile('Z:\Julius\Test_Processing\Results', filesep);

%Get the list of regions
regionDir = dir([inDir '*Drift*']);

%Process them one by one
for regionDirIdx = 1: length(regionDir)
    if regionDir(regionDirIdx).isdir == 1
        inDirReg = fullfile(inDir, regionDir(regionDirIdx).name, 'Registered', filesep);
        disp(['Processing_RegionID = ' num2str(regionDirIdx) ': ' inDirReg]);
        fnListChr = dir([inDirReg '*Chr*.tif']); %Name of the input file - DNA
        
        if isempty(fnListChr)
            disp('No input file for DNA is found');
            continue;
        end
        
        fpRegFlag = fopen(fullfile(inDir, regionDir(regionDirIdx).name, 'region_type_flag.txt'),'r');
        if fpRegFlag == -1
            continue;
        else
            curLine = fgets(fpRegFlag);
            regFlag = sscanf(curLine, '%d');
        end
        
        %Select the right translation value
        %Note: X Y indices between Zen and Matlab swap!!!
        if regFlag == 1
            shiftX = -1; 
            shiftY = 5;
            shiftZ = 0;
        elseif regFlag == 2
            shiftX = 0; 
            shiftY = 0;
            shiftZ = 0;
        elseif regFlag == 3
            shiftX = 0; 
            shiftY = 0;
            shiftZ = 0;
        elseif regFlag == 4 || regFlag == 5
            shiftX = 7; 
            shiftY = 1;
            shiftZ = 0;
        else
            continue;
        end
        
        %Directory to save shift corrected results
        outDirReg = fullfile(inDir, regionDir(regionDirIdx).name, 'Registered_ShiftCorr', filesep);
        if ~exist (fullfile(outDirReg, fnListChr(1).name)) || reProcShift == 1
            [chrStackReg] = fn_open_tiff_stack(inDirReg, fnListChr(1).name);
            [chrStackSht] = fn_image_translate_xy(chrStackReg, [shiftX shiftY]);
            
            if ~exist(outDirReg)
                mkdir(outDirReg);
            end
            fn_write_tiff_stack(chrStackSht, outDirReg, fnListChr(1).name, 16);
            
            inDirBinMask = fullfile(inDir, regionDir(regionDirIdx).name, 'Segmented_BinMask', filesep);
            outDirBinMask = fullfile(inDir, regionDir(regionDirIdx).name, 'Segmented_BinMask_ShiftCorr', filesep);
            [chrBinMask] = fn_open_tiff_stack(inDirBinMask, fnListChr(1).name);
            [chrBinMaskSht] = fn_image_translate_xy(chrBinMask, [shiftX shiftY]);
            
            if ~exist(outDirBinMask)
                mkdir(outDirBinMask);
            end
            fn_write_tiff_stack(chrBinMaskSht, outDirBinMask, fnListChr(1).name, 16);
            
            inDirOverlaid = fullfile(inDir, regionDir(regionDirIdx).name, 'Segmented_Overlaid', filesep);
            outDirOverlaid = inDirOverlaid;
            [chrOverlaid] = fn_open_tiff_stack(inDirOverlaid, fnListChr(1).name);
            [chrOverlaidSht] = fn_image_translate_xy(chrOverlaid, [shiftX shiftY]);
            
            flag = fn_write_tiff_stack(chrOverlaidSht, outDirOverlaid, fnListChr(1).name, 16);
        end
    end
end

