function [outImage] = fn_DrawSphere(inImage, cx, cy, cz, r, rx,ry, rz, lineFlag)

[dx,dy,Nz] = size(inImage);
outImage = inImage;
for i=cx-r:cx+r
    for j =cy-r:cy+r
        for k =cz-r:cz+r
           if i>0 && i<=dx && j>0 && j<=dy && k>0 && k<=Nz 
               if sqrt(((cx-i)*rx)^2 + ((cy-j)*ry)^2 + ((cz-k)*rz)^2) <= r
                   outImage(i,j,k) = 1;
               end
           end
        end
    end
end
if lineFlag == 1
    outImage(cx,cy,1:cz) =1;
end
end

