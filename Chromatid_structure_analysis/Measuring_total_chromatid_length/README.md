This function calculates the total chromatid length of a cell by combining manual and automated segmentation.
The function takes the chromatid and condensin volume as input in addition to the binary mask of a manually segmented portion of a chromatid. It saves the total chromatid length as output.

