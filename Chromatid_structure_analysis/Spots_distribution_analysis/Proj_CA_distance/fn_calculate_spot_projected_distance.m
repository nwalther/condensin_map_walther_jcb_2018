function [projXCoords_pix, projYCoords_pix, projDist_pix] = fn_spot_ca_projected_distance_indiv_region(xCoords_pix, yCoords_pix, centAxisX_pix, centAxisY_pix)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
projXCoords_pix = size(xCoords_pix);
projYCoords_pix = size(yCoords_pix);

centAxis = [centAxisX_pix centAxisY_pix];
centAxisFlag = zeros(size(centAxisX_pix));
projDist_pix = zeros(size(xCoords_pix));
for spotId = 1: length(xCoords_pix)
    curSpot = [xCoords_pix(spotId) yCoords_pix(spotId)];
    
    distance = sqrt(sum((centAxis-curSpot).^2,2));
    [~, idx] = min(distance);
    projXCoords_pix(spotId) = centAxisX_pix(idx);
    projYCoords_pix(spotId) = centAxisY_pix(idx);
    centAxisFlag(idx) = centAxisFlag(idx) + 1;
end

prevIdx = -1;
distCount = 0;
for curAxisPoint = 1: length(centAxisFlag)
    if centAxisFlag(curAxisPoint) > 0
        if prevIdx == -1
            prevIdx = curAxisPoint;
            prevPoint = [centAxisX_pix(curAxisPoint) centAxisY_pix(curAxisPoint)];
        else
            %curIdx = curAxisPoint;
            curPoint = [centAxisX_pix(curAxisPoint) centAxisY_pix(curAxisPoint)];
            
            distance = sqrt(sum((prevPoint-curPoint).^2,2));
            distCount = distCount +1;
            
            projDist_pix(distCount) = distance;
            if centAxisFlag(curAxisPoint) > 1
                distCount = distCount + centAxisFlag(curAxisPoint) -1;
            end
            %prevIdx = curAxisPoint;
            prevPoint = curPoint;
        end
    end    
end
end

