%This script processes all input all condensin regions by calling the 
%function (fn_calculate_chromatid_central_axis) that caculates the central 
%axis of chromatid for indidividual chromatid regions.
%Author: M. Julius Hossain, EMBL
%Last update: 2017_09_14

clear all;
close all;
clc;
%Go two step upper parent directory to get the source root directory
[parentdir,~,~]=fileparts(pwd);
[src_root_dir,~,~]=fileparts(parentdir);
addpath(fullfile(src_root_dir, 'common_functions'));
%addpath 'C:\Research Materials\Codes\Nike\STED_condensin_structural_analysis';

%voxel size in micrometer
voxelSizeX = 0.02;
voxelSizeY = 0.02;
voxelSizeZ = 0.14;

%Root directory containing individual chromatid regions
inDirRoot = fullfile('X:\Nike\Abberior-STED\RegionsAnalysis_SingleColour\Results', filesep);
%Root directory to save the results
outDirRoot = 'Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Central_axis\';
%Output directory to store max projected image with spot
outDirMaxProj = 'Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Proj_bin_mask\';

%Create output directory to store max projected image with spot
if ~exist(outDirMaxProj)
    mkdir(outDirMaxProj)
end

%Get the region directories
regionDir = dir([inDirRoot '*Drift*']);

%Process individual regions
succProcId = 1; %count the number of successfully processed regions
for regionDirIdx = 1: length(regionDir)
    if regionDir(regionDirIdx).isdir == 1
        curRegionDir = fullfile(inDirRoot, regionDir(regionDirIdx).name, filesep);
        try
            disp(['Processing: ' regionDir(regionDirIdx).name]);
            %fn_generate_max_projected_condensin(curRegionDir, outDirRoot);
            procStat = fn_calculate_chromatid_central_axis(curRegionDir, outDirRoot, outDirMaxProj, succProcId, voxelSizeX, voxelSizeY, voxelSizeZ);
            succProcId = succProcId + procStat; 
            close all;
        catch
            disp(['Could not process: '  regionDir(regionDirIdx).name]);
        end
    end
end