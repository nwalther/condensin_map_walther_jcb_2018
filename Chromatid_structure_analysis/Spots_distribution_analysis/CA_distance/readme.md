This module calculates the distance of a detected spot to the central condensin axis (CA). 
It then combines the results per condensin subunit and mitotic phase.

To run the pipeline:
*Set input and output directories
*Execute calculate_chromatid_central_axis_main.m
*Execute Generate_max_projected_condensin_main.m
*Execute Generate_spot_to_central_axis_distance.m
*Execute Generate_spot_CA_dist_all_region.m