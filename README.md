# Source code to Walther et al. 2018 

>**A quantitative map of human Condensins provides new insights into mitotic chromosome architecture**
Nike Walther, M. Julius Hossain, Antonio Z. Politi, Birgit Koch, Moritz Kueblbeck, Oeyvind Oedegaard-Fougner, Marko Lampe, Jan Ellenberg bioRxiv 237834; doi: https://doi.org/10.1101/237834 
Walther et al. 2018


The different folders contain code for a specific processing task. 


## Requirements
The code has been tested on the following versions. Older versions of the software may also work.
* MATLAB (>= 2017a, image processing toolbox Version >= 10.0)
* Bioformat for MATLAB (http://downloads.openmicroscopy.org/bio-formats/5.3.4/)
* Recent FiJi installation (ImageJ 1.51s, Java 1.8.0_66)
* RStudio/R installation (R >= 3.4.1)


## Chromatid_structure_analysis
This code is used for the analysis of mitotic chromatid structure.

### Manual-segmentation_tiffstack
This script allows the user to draw an irregular shape over a gray scale image in order to extract the region of interest and save the segmented result as a binary mask.

### STED_image_analysis_single_colour
This module performs structural analyses on condensin regions: width and radial profile.

### STED_image_analysis_double_colour
This module performs a colocalization analysis between two condensin channels. 

### Spots_distribution_analysis
This module calculates NN, CA, AS, of Condensin subunits based on the coordinates of spots detected and clustered with zStackSpotPicker.

### Measuring_total_chromatid_length
This function calculates the total chromatid length of a cell by combining manual and automated segmentation.

### common_functions
Contains some common functions in different modules.

### external_functions
This folder contains functions from external sources which are available in public and have been used in some of the modules.


## FRAP
Process fluorescence after photobleaching experiments. See the [README](./FRAP/README.md) file in the respective folder for a detailed explanation.

### image\_analysis
Contains an ImageJ jython script (`code\frapmitotichr.py`) to process the images and extract fluorescence intensity profiles along the chromatin. Example data can be found in `FRAP\image_analysis\example_data`. 

### data\_analysis
The folder `code` contains R packages and scripts to analyse the fluorescence profile and fit mathematical models to the data in order to extract residence times and immobile fractions. The folder `data` contains the profiles for different cells and POIs. 

## MitoSys
This folder contains and links to code used to analyse FCS-calibrated confocal time-lapse data through mitosis and generate time-resolved cellular protein concentration and number maps.


## Segmentation_single_cell
This code segments chromosome and cell boundary of a cell at a single time point.


## zStackSpotPicker
This is a collection of three scripts that open a z-stack, find spot-like structures in this stack, use a DBscan algorithm to cluster the spots from different planes together, and make a plot of the result.

### 01_spotPicker_Nike_commented.ijm
This script can be opened and run in ImageJ if Thunderstorm (https://github.com/zitmen/thunderstorm) is installed. It runs the Thunderstorm picking algorithm with parameters optimized for this type of 2D STED data.

### 02_zClustersDB_calculations.R
This script uses the output from 01_spotPicker_Nike_commented.ijm and merges spots that are close together in xyz into clusters.

### 03_zClustersDB_plotting.R
This script can be used to plot the spots coloured by the cluster they belong to.


## Authors
M. Julius Hossain, EMBL, Heidelberg

Antonio Politi, EMBL, Heidelberg

Oeyvind Oedegaard-Fougner, EMBL, Heidelberg

