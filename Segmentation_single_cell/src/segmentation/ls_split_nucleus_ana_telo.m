function [ ncRegion, ncRegion1 ] = ls_split_nucleus_ana_telo(ncRegion,voxelSizeX, voxelSizeY, voxelSizeZ)
%This function divides two merged nucleus which were detected separately before
[dx,dy,Nz] = size(ncRegion);
[x,y,z] = ls_threed_coord(find(ncRegion),dx,dy);
[V, R, xg1, yg1, zg1] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ);
Vma1 = V(:,1);
t = -200:0.25:200;
xma1 = t*Vma1(1)+xg1;
yma1 = t*Vma1(2)+yg1;
zma1 = t*Vma1(3)+zg1;

Vma2 = V(:,2);
xma2 = t*Vma2(1)+xg1;
yma2 = t*Vma2(2)+yg1;
zma2 = t*Vma2(3)+zg1;
midPlane = ones(size(ncRegion));

for i = 1: size(xma1,2)
    yma2cur = round((yma2 - yma1(i) + yg1)/voxelSizeY);
    xma2cur = round((xma2 - xma1(i) + xg1)/voxelSizeX);
    zma2cur = round((zma2 - zma1(i) + zg1)/voxelSizeZ);
    for j = 1: size(xma2cur,2)
        if xma2cur(j)>= 1 && xma2cur(j) <=dx && yma2cur(j)>= 1 && yma2cur(j) <=dy && zma2cur(j)>= 1 && zma2cur(j) <=Nz
            midPlane(xma2cur(j),yma2cur(j),zma2cur(j)) = 0;
        end
    end
end

midPlane = imerode(midPlane, ones(3,3,3));
ncRegion = and(ncRegion, midPlane);

threeDLabel = bwconncomp(ncRegion, 18);
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
[biggest, idx] = max(numPixels);
ncRegion(:,:,:) = 0;
ncRegion(threeDLabel.PixelIdxList{idx}) = 1;
numPixels(idx) = 0;
[biggest, idx] = max(numPixels);
ncRegion1 = zeros(size(ncRegion));
ncRegion1(threeDLabel.PixelIdxList{idx}) = 1;
clear midPlane;
end