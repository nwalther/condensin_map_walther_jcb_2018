function [marker] = ls_merge_small_regions(chRegion, marker, bordImg, voxelSizeX, voxelSizeY, voxelSizeZ, minVol)
%This functions connects regions that are part of chromosome.
% disRow = 5;
% disCol = 5;

%% Combining distance transform and watershed to identify cell region of interest
tempVol = zeros(size(marker));
labelCh = zeros(size(chRegion));
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ;

threeDLabelCh = bwconncomp(chRegion, 18);
threeDLabelMk = bwconncomp(marker, 18);

for i=1: threeDLabelCh.NumObjects
    labelCh(threeDLabelCh.PixelIdxList{i}) = i;
end

mkBelongToCh = zeros(threeDLabelCh.NumObjects,1);
for i=1: threeDLabelMk.NumObjects
    tempVol(:,:,:) = 0;
    tempVol(threeDLabelMk.PixelIdxList{i}) = 1;
    mkBelongToCh(i) = round(sum(sum(sum(tempVol .* labelCh)))/sum(sum(sum(tempVol))));
end
    
for i=1: threeDLabelMk.NumObjects
    blobVol = length(threeDLabelMk.PixelIdxList{i}) * voxelSize;
    if blobVol ~= 0 && sum(sum(sum(tempVol .* bordImg))) ~= 0  && blobVol<minVol
        [x1,y1,z1] = ind2sub(size(marker),threeDLabelMk.PixelIdxList{i});
        minDist=10000;
        for j=1:threeDLabelMk.NumObjects
            if i ~=j && mkBelongToCh(j) == mkBelongToCh(i) && ~isempty(threeDLabelMk.PixelIdxList{j})
                [x2,y2,z2] = ind2sub(size(marker),threeDLabelMk.PixelIdxList{j});
                [blobDist,matching_coordinates] = calculate_min_distance([x1,y1,z1],[x2,y2,z2]);
                if blobDist < minDist
                    minIndex = j;
                    minDist = blobDist;
                    matchCoords = matching_coordinates;
                end
            end
        end
        if minDist <=3
            drawLine(matchCoords, marker);
            threeDLabelMk.PixelIdxList{minIndex} = [threeDLabelMk.PixelIdxList{minIndex};  threeDLabelMk.PixelIdxList{i}];
            threeDLabelMk.PixelIdxList{i} = [];
        end
    end
end
                
            
    
%displaySubplots(distImage, 'DT image', disRow, disCol, Nz, zFactor, 2);

%Display marker image
%displaySubplots(marker, 'CH marker', disRow, disCol, Nz, zFactor, 2);


%displaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
clear tempVol;
end

function marker = drawLine(matchCoords, marker)
    if size(matchCoords, 1) > 0 && size(matchCoords,2) == 6
        x1 = matchCoords(1,1);
        y1 = matchCoords(1,2);
        z1 = matchCoords(1,3);
        x2 = matchCoords(1,4);
        y2 = matchCoords(1,5);
        z2 = matchCoords(1,6);
        for t = 0.25:0.25:0.75
            x = round(x1 *(1-t) +x2*t);
            y = round(y1 *(1-t) +y2*t);
            z = round(z1 *(1-t) +z2*t);
            marker(x, y, z) = 1;
        end
    end        
end
