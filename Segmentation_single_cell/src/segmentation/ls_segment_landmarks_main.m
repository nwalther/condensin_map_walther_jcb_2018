function ls_segment_landmarks_main
% This function segments lanmarks - chromosome  and cell (optional)volumes and extracts
% different parameters from the segmented data

% Author: M. Julius Hossain, EMBL Heidelberg.
% Created: 2017-06-09
% Last update: 2017-06-13

clc; clear all; close all;

addpath 'segmentation_external_code';

segCellBoundary = 1; % Set 0 to segment chromosome only
dispSegMass = 1; % Set 1 to save 3d reconstructed chromosome.
nRemLowerSlices = 0; % number of lower slices to exclude from segmentation
chrSigInDxt = 0; % Set 1 if chromosome signal is also prominent in dextral channel
cellChanIdx = 2; % Channel index of dextran
chrChanIdx  = 3; % Channel index of chromatin
exParamsFn  = 'Extracted_parameters.txt'; % Name of the file to save the extracted parameters
expDirRoot = uigetdir('','Select the root directory of your data (expDirRoot)');
%Input data are expected to be in the subdirectories under expDirRoot
%expDirRoot = 'Y:\Julius\Data\Test_chr_cellBound_segmentation\';
outDirRoot = uigetdir('','Select the root directory to store the results (outDirRoot)');
%outDirRoot = 'Y:\Julius\Data\Result_test_chr_cellBound\';
cellDirs = dir(expDirRoot);

for i = 3: length(cellDirs)
    if cellDirs(i).isdir == 1
        curInDir  = fullfile(expDirRoot, cellDirs(i).name, filesep);
        curOutDir = fullfile(outDirRoot, cellDirs(i).name, filesep);
        if ~exist(curOutDir)
            mkdir(curOutDir);
        end
        if segCellBoundary == 1
            curOutDirMarker = fullfile(curOutDir, 'ChrMarker', filesep);
            %This directory saves the marker from chromosome segmentation that is
            %used for cell boundary detection in later stage
            if ~exist(curOutDirMarker)
                mkdir(curOutDirMarker)
            end
        end
        segParamsDir = fullfile(curOutDir, 'SegParams', filesep);
        if ~exist(segParamsDir)
            mkdir(segParamsDir);
        end
                if segCellBoundary == 1
                    chrProcStat = ls_detect_chromosomes(curInDir, curOutDir, segParamsDir, exParamsFn, chrChanIdx, dispSegMass, nRemLowerSlices, curOutDirMarker);
                else
                    chrProcStat = ls_detect_chromosomes(curInDir, curOutDir, segParamsDir,  exParamsFn, chrChanIdx, dispSegMass, nRemLowerSlices);
                end
        
        chrProcStat = 1;
        if  segCellBoundary && chrProcStat == 1
            cellProcStat = ls_detect_cell_membrane(curInDir, curOutDir, curOutDirMarker, exParamsFn, cellChanIdx, chrChanIdx, nRemLowerSlices, chrSigInDxt, dispSegMass);
        end
    end
end

end

