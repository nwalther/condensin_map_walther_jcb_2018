## zStackSpotPicker
This is a collection of three scripts that open a z-stack, find spot-like structures in this stack, use a DBscan algorithm to cluster the spots from different planes together, and make a plot of the result.

### 01_spotPicker_Nike_commented.ijm
This script can be opened and run in ImageJ if Thunderstorm (https://github.com/zitmen/thunderstorm) is installed. It runs the Thunderstorm picking algorithm with parameters optimized for this type of 2D STED data.

### 02_zClustersDB_calculations.R
This script uses the output from 01_spotPicker_Nike_commented.ijm and merges spots that are close together in xyz into clusters.

### 03_zClustersDB_plotting.R
This script can be used to plot the spots coloured by the cluster they belong to.

## Author
Oeyvind Oedagaard-Fougner, EMBL, Heidelberg