// This script is used to localize spots in stacks of tiff files
// Written by Øyvind Ødegård  - Oeyvind Oedegaard oeyvind.oedegaard@embl.de
// Requires ImageJ, Thunderstorm, a folder with z-stackes

// File handeling
run("Close All"); // Close all ImageJ windows
dir = getDirectory("Choose an input directory"); // Selected folder Registered

// create a timestamp to be used in your output folder
getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
TimeString = toString(year);
if (month<10) {TimeString = TimeString+"0";}
TimeString = TimeString + month;
if (dayOfMonth<10) {TimeString = TimeString+"0";}
TimeString = TimeString + dayOfMonth;

// generate a output folder
dir_out = substring(dir,0,lengthOf(dir)-1) + "_IJ_"+ TimeString +"/"; // If you want to to brose the target dir use // getDirectoryTarget Directory
if (File.exists(dir_out)) {
	print("files in target folder will be overwritten");
	// read README.txt file and remove files that are in the list there.
	}else{
		File.makeDirectory(dir_out);
		print("new output folder created: " + dir_out);
}

// Find files in file dir
list = getFileList(dir); // saves the path of all the files in the folder

for (i=0; i<list.length; i++) { // start a for loop that starts from 0 to the number of files in the folder
     if (endsWith(list[i], ".tif")){ // will do the following commands on only the .tif files
        print(i + ": " + dir+list[i]); // Prints a list of all the files
        open(dir+list[i]); // Opens file i in the list
        run("Grays"); // adjust the lookup table to grey, for the user to see it better
            
        imgName=getTitle(); // saves the name of the file
        
        //  make an absolute path to the output file
        outCsvName = dir_out + imgName + "loc.csv";
        //outTiffName = dir + imgName + "out.tif";
        //outStackName2D = dir_out + imgName + "_picks_2Dprojection.tif";
        outStackName3D = dir_out + imgName + "_picks_3D.tif";

        //print(imgName);

        // Change camera setup
        run("Camera setup", "readoutnoise=0.0 offset=0.0 quantumefficiency=1.0 isemgain=false photons2adu=1.0 pixelsize=20.0");

        // Starting Thunderstorm 
        
		run("Run analysis", "filter=[Wavelet filter (B-Spline)] scale=1.5 order=3 detector=[Local maximum] connectivity=8-neighbourhood threshold=3*std(Wave.F1) estimator=[PSF: Integrated Gaussian] sigma=2.2 fitradius=2 method=[Maximum likelihood] full_image_fitting=false mfaenabled=false renderer=[Averaged shifted histograms] magnification=1.0 colorize=false threed=false shifts=2 repaint=0");

        // Save the localization in the same folder
        run("Export results", "filepath=["+outCsvName+"] file=[CSV (comma separated)] "+
        "frame=true x=true y=true sigma=true intensity=true offset=true bkgstd=true uncertainty=true");
        print("saved sucsessfully as: " + outCsvName);

		// Save a 2D maximum projection of the picks
        //selectWindow("Averaged shifted histograms");
        //saveAs("Tiff", outStackName2D);
        //run("Close");

        // Save a 3D stack of the image with selections 	
      	selectWindow(imgName);
      	setSlice(nSlices/2);
      	run("Enhance Contrast", "saturated=0.35"); 
      	
      	run("Flatten", "stack");
      	
      	saveAs("Tiff", outStackName3D);
      	run("Close All");

     //  
     }
} 
print("Finished");
//run("Close All");
