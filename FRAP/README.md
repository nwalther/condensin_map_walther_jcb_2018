# Fluorescence recovery after photobleaching (FRAP) of mitotic cells



Code to perform image and data analysis of FRAP experiments. A chromatin binding protein of interest (POI) has been bleached on half of the mitotic chromosomes. 

This code has been used to generate Figure S2E and Figure S2 I-L of [Walther et al. 2018](https://www.biorxiv.org/content/early/2018/01/09/237834).



## FRAP\image\_analysis directory



The Jython ImageJ script ``code\frapmitoticchr.py`` extracts the fluorescence profile of the POI and chromatin along the main axis of the chromatin. This data is then used in the data analysis step to extract FRAP curves and fit models to the data. 



### Installation and usage

* Install [FiJi](https:\\fiji.sc)

* Drag and drop ``code\frapmitoticchr.py`` on the FiJi window, this should cause the opening of an editor 

* Press `Run` 

* The program will prompt for a FRAP database file. An example data set can be found in `example_data`. Select the file named `example_data\database_frap.txt`.



The format of the file is a tab delimited file which gives the path to the images (one row per image). 

```

ID	POI	Cellline	path	iter	manualQC	processuptoframe	bleach	segmentation	filter_size	method	exclude	comments

1	CAP-H2	NCAPH2gfpc67	.\20170614-15_Condensins_FRAP\NCAPH2gfpc67\100perc488_100it_20s_x31\cell0002	100	1	0	1	0	3	Default	1	

```

Script steps  | Schematic illustration
:-------| :-------------
Segment chromatin per frame (Gaussian blur and Thresholding). <br/>Compute 3D connected components and remove small objects <br/>Find main axis of chromatin in each frame and create line roi (several pixels wide) along main axis <br/>Optionally apply anisotropic diffusion filter <br/> Apply binary mask to chromatin and POI <br/> Extract data using the line ROI <br/> Save profile data to file | The segmentation <br/> <img src="./resources/frap_analysis_method-1.png" width = "400px"> 


### Outcome

The script generates three tab delimited files either named 

* `imagename_aniDiff_poi.txt`, `imagename_aniDiff_chr.txt` `imagename_aniDiff_mask.txt` if the anisotropic diffusion filter is used or

* `imagename_poi|chr|mask.txt` otherwise.



The files contain the extracted average intensity along the line roi for the POI, the chromatin and the binary mask. The binary mask values are used in the next step to compute a value that is proportional to the concentration of POI on chromatin. The location of the line Rois and the binary mask are saved to `imagename_..._roi.zip` and `imagename_..._mask.tif`, respectively.



## FRAP\data\_analysis directory

R scripts and packages to analyse the profiles obtained from the image analysis. Code is included in the `code` directory. Profiles of POI and chromatin for different experiments are included in the `data` directory. 



### Installation and usage

Install [RStudio](https://www.rstudio.com/).  Then install  the two packages `frapfun` and `mitosysproteindist`  following these instructions:

* Load the package `code\frapfun` by clicking on `frapfun.Rproj`. This packages contains function to average the intensity profiles

* Go to the `Build` tab on the right window of RStudio and click `Install and Restart` 

![install_and_restart](./resources/compile_package.png)

* Load the package `code\mitosysproteindist` by clicking on `mitosysproteindist.Rproj`. This package contains functions to process data from the mitosys pipeline

* Click on `Install and Restart`



Load the file `FRAP_analysis.Rmd` and execute the different steps. The R notebook loads all the data included in the `data` directory, extract and process the profiles. After all steps have been executed, save the file. An html version of the results is `FRAP_analysis.nb.html`.



Script steps  | Schematic illustration
:-------| :-------------
Compute the average profile from several Z-slices (in the example Z=4,5,6). <br/>Compute weighted mean fluorescence intensities in unbleached and bleached region. <br/> Compute the normalized difference and fit different models to the data.  | POI and chromatin profiles  below <img src="./resources/frap_analysis_method-2.png" width = "400px"> <br/> Mean POI intensities <br/> <img src="./resources/frap_analysis_method-3.png" width = "400px"> 



### Outcome

The script will generate a `results` directory and numerous files and figures. The files are tab delimited files giving the distribution of the kinetic parameters. 



## Author

Antonio Politi, EMBL, 2017

