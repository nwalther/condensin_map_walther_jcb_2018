#' Calibrated protein distribution example obtained from mitosys Pipeline 
#' 
#' @description   
#' The dataset contains the amount of several condensin subunits in cytoplasm and chromatin mass 
#' \href{https://www.biorxiv.org/content/early/2018/01/09/237834}{Walther et al. 2018} 
#' Image data has been processed and time aligned to a mitotic standard time using the MitoSys pipeline 
#' \href{https://www.biorxiv.org/content/early/2017/12/01/227751}{Cai et al. 2017} 
#' and FCS-calibrated imaging \href{https://www.biorxiv.org/content/early/2018/02/22/188862}{Politi et al. 2018}.
#' @usage mitosysproteindist::bgcorrect_totalN(proteinDist) 
#' @format A data frame with several rows and 41 variables:
#' \describe{
#'     \item{POI}{name of POI}
#'     \item{CellID}{Cell ID}
#'     \item{cellpath}{relative filepath to result cell directory}
#'     \item{filepath}{absolute filepath to image}
#'     \item{frame}{imaging frame}
#'     \item{timeMin}{imaging time}
#'     \item{mitoTimeJH}{classification from segmentation pipeline only. Geometry change in chromatin}
#'     \item{mitoTimeMin}{Mitotic standard time}
#'     \item{mitoTimeStage}{mitotic standard stage}
#'     \item{numChr}{number of chromosomes from segmentation pipeline only}
#'     \item{POI_cell12_N}{total number of proteins in cell POI_cell12_nM*Vol_cell12*Na*Vvoxel}
#'     \item{POI_cyt12_N}{total number of proteins in cytoplasm POI_cyt12_nM*Vol_cyt12*Na*Vvoxel}
#'     \item{POI_nuc12_N}{total number of proteins in nuclei (1+2)  POI_nuc12_nM*Vol_nuc12*Na*Vvoxel}
#'     \item{POI_nuc1_N}{total number of proteins in nucleus 1  POI_nuc1_nM*Vol_nuc1*Na*Vvoxel}
#'     \item{POI_nuc2_N}{total number of proteins in nucleus 2  POI_nuc2_nM*Vol_nuc2*Na*Vvoxel}
#'     \item{POI_cell12_nM}{average concentration of POI in cell}
#'     \item{POI_cyt12_nM}{average concentration of POI in cytoplasm}
#'     \item{POI_nuc12_nM}{average concentration of POI in nucleus 1 and 2}
#'     \item{POI_nuc1_nM}{average concentration of POI in nucleus 1}
#'     \item{POI_nuc2_nM}{average concentration of POI in nucleus 2}
#'     \item{Vol_cell12_um3}{volume of cell(s) in um3}
#'     \item{Vol_cyt12_um3}{volume of cytoplasm in um3}
#'     \item{Vol_nuc12_um3}{volume of nuclei in um3 (= nuc1 + nuc2 or nuc1 if not yet divided)}
#'     \item{Vol_nuc1_um3}{volume of nucleus 1 in um3}
#'     \item{Vol_nuc2_um3}{volume of nucleus 2 in um3}
#'     \item{calibration_P1}{calibration factor P1 (baseline)}
#'     \item{calibration_P2}{calibration factor P2 (slope)}
#'     \item{calibration_P1_wt}{calibration factor P1 (baseline) estimated from WT measurement}
#'     \item{Veff_fl}{effective confocal volume in fl}
#'     \item{w0_um}{effective confocal radius in um}
#'     \item{kappa}{structural parameter of effective confocal volume}
#'     \item{POI_cell12_I}{total fluorescence intensity cell}
#'     \item{POI_cyt12_I}{total fluorescence intensity cytoplasm}
#'     \item{POI_nuc12_I}{total fluorescence intensity nuclei (1+2)}
#'     \item{POI_nuc1_I}{total fluorescence intensity in nucleus 1}
#'     \item{POI_nuc2_I}{total fluorescence intensity in nucleus 2}
#'     \item{Vol_cell12_px}{volume cell 1 + 2 in pixels}
#'     \item{Vol_cyt12_px}{volume cytoplasm 1 + 2 in pixels}
#'     \item{Vol_nuc12_px}{volume nuclei 1 + 2 in pixels}
#'     \item{Vol_nuc1_px}{volume nucleus 1 in pixels}
#'     \item{Vol_nuc2_px}{volume nucleus 2 in pixels}}
"proteinDist"
